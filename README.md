# Cobot Buffing Showcase

This site showcases the Cobot Buffing application being developed with DuroByte and Plasman. 

![](img/2021-09-28-15-00-08.png)

![](img/2021-09-28-15-00-59.png)

![](img/2021-09-28-15-01-21.png)

![](img/2021-09-28-15-01-54.png)


## Part
License plate surround. 

![](img/2021-08-27-15-26-54.png)




## Sequence of Operations
The sequence of operations for the station will change from the current manual operation to a cobot assisted operation. It is understood in the industry that robotic buffing usually requires operator assistance to complete the buffing operation to typical manual operation standards of quality.

1. The part is loaded manually by the operator into the provided part fixture.
<!-- 2. Sensors will determine that the parts are seated. -->
1. An operator will apply buffing compound to the buffing head.
1. The robot is started by the operator by pressing the start pushbutton.
1. The robot advances to the part and executes the buffing path.
1. Upon completion, the robot returns home.
1. The indicator on the PB station flashes green to prompt the operator to unload the part.
1. The operator removes the part and the cycle is complete.
1. The operator may transition the part to a manual buffer station to complete the buffing, if deemed necessary.

## Performance
Anticipated cycle time is roughly 40 seconds. This is based on previous application knowledge. 

## Hardware
- [UR5e Robot](https://www.universal-robots.com/products/ur5-robot/)
- MIRKA Specific Buffing Head with Orbital Rotation `KITAIROP77`
  - [AIROS/AIROP](https://www.mirka.com/airos/)

## Consumables
- Polishing compound: Mirka
- Polishing pads: Mirka

## Cell Concept 

![concept](img/concept.drawio.png)

![](img/2021-09-28-07-51-27.png)

[Polisher Video](https://photos.app.goo.gl/JnGoqnGoRuUkaXxi6)

[Simulation Video](img/21227_Simulation_loop.mp4)

## Fixture Design

- Minimize potential for buffing head damage (sharp edges).
  - Consider filling bottom cavities in part with fixture bosses.
- Ensure robot can reach all parts of the part without awkward movements.
  - Consider using simulation.
  - Consider not placing the robot in the center of the part.

## Software
- All control will be done on the URobot.
- Operation interaction via push buttons, pilot lights and robot pendant (display only). No additional HMI required.


## Environment
- Power via (2) 120VAC receptacles.
